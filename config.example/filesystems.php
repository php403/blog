<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'qiniu'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

        'upyun' => [
            'driver'        => 'upyun',
            'bucket'        => env('UPYUN_BUCKET'),
            'operator'      => env('UPYUN_OPERATOR'),
            'password'      => env('UPYUN_PASSWORD'),
            'domain'        => env('UPYUN_DOMAIN'),
            'protocol'      => env('UPYUN_PROTOCOL'),
        ],
        'qiniu' => [
            'driver'  => 'qiniu',
            'domains' => [
                'default'   => 'qiniu.php403.cn', //你的七牛域名
                'https'     => 'qiniu.php403.cn',         //你的HTTPS域名
                'custom'    => 'qiniu.php403.cn',                //你的自定义域名
            ],
            'access_key'=> 'L6NSngbfwxkAe-qdwOPMS_R74LUcg4Yj2mGdMltX',  //AccessKey
            'secret_key'=> 'xOarKHO30lyHHVCls-6OUdsMgsG2G3XuvTibYRZD',  //SecretKey
            'bucket'    => 'pgblog',  //Bucket名字
            'notify_url'=> '',  //持久化处理回调地址
        ],

    ],

];
